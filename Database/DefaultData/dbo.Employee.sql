﻿BEGIN
INSERT INTO [dbo].[Employee] ( [Name], [Salary], [Status], [PositionID]) VALUES ( N'Andrew', 3500, 0, 5)
INSERT INTO [dbo].[Employee] ( [Name], [Salary], [Status], [PositionID]) VALUES ( N'Maria', 15000, 1, 4)
INSERT INTO [dbo].[Employee] ( [Name], [Salary], [Status], [PositionID]) VALUES ( N'Artur', 25000, 1, 2)
INSERT INTO [dbo].[Employee] ( [Name], [Salary], [Status], [PositionID]) VALUES ( N'Damian', 30000, 1, 1)
INSERT INTO [dbo].[Employee] ( [Name], [Salary], [Status], [PositionID]) VALUES ( N'Yakov', 10000, 1, 4)
INSERT INTO [dbo].[Employee] ( [Name], [Salary], [Status], [PositionID]) VALUES ( N'Polina', 5500, 1, 3)
INSERT INTO [dbo].[Employee] ( [Name], [Salary], [Status], [PositionID]) VALUES ( N'Liza', 8000, 0, 4)
INSERT INTO [dbo].[Employee] ( [Name], [Salary], [Status], [PositionID]) VALUES ( N'Monna', 3000, 0, 5)
END