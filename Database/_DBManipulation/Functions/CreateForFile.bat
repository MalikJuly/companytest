SET Error=0

IF "%2"=="RunOrder.cfg" (
	@ECHO 	skip %2
	GOTO END
)


IF EXIST %1\%2\*.sql (
	ECHO %1\%2 >> con
	CALL %FuncsDir%\CreateForFolder.bat %1\%2
	GOTO OK
)

ECHO ...%1\%2 >> con

@ECHO 	...%2

COPY %1\%2 %1\%2.rsql>nul
%SqlCmdString% -i%1\%2.rsql
IF NOT "%ERRORLEVEL%"=="0" SET Error=1
DEL /F %1\%2.rsql
:END

IF NOT "%Error%"=="0" GOTO ERR
GOTO OK

:ERR
ECHO.
ECHO.
ECHO ******************************************************************8
ECHO [%1] NOT COMPLETED
ECHO somethings happening here: %1\%2
ECHO for details see logs above
EXIT 111 

:OK
