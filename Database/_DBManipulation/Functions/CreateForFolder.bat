ECHO.

IF NOT EXIST %1\RunOrder.cfg GOTO ProcessAll

:ProcessSpecifed
ECHO Create on specifed order in [%1]
@FOR /F "tokens=*" %%i IN (%1\RunOrder.cfg) DO (	
	CALL %FuncsDir%\CreateForFile.bat %1 %%i
)
GOTO END

:ProcessAll
ECHO Create for all in [%1]
@FOR /F "tokens=*" %%i in ('DIR /B /ON %1\*.sql') DO (	
	CALL %FuncsDir%\CreateForFile.bat %1 %%i
)

:END
ECHO [%1] Completed
ECHO.
