@ECHO OFF
SETLOCAL
REM *************************************************
REM *** Util for Create/Update database           ***
REM *** Note: For change params see ../Config.cfg ***
REM *************************************************

SET WorkDir=_DBManipulation
SET FuncsDir=%WorkDir%\Functions

REM reading params
CALL %FuncsDir%\GetParamFromConfig.bat DatabaseServer
CALL %FuncsDir%\GetParamFromConfig.bat DatabaseUser
CALL %FuncsDir%\GetParamFromConfig.bat DatabasePassword
CALL %FuncsDir%\GetParamFromConfig.bat DatabaseDBName
CALL %FuncsDir%\GetParamFromConfig.bat SQLClient
CALL %FuncsDir%\GetParamFromConfig.bat IgnoreErrors
CALL %FuncsDir%\GetParamFromConfig.bat RunUpdate
CALL %FuncsDir%\GetParamFromConfig.bat RunDefaultData
CALL %FuncsDir%\GetParamFromConfig.bat RunDemoData
CALL %FuncsDir%\GetParamFromConfig.bat RunCreateDB


REM sql client check
IF NOT EXIST %SQLClient% (
	ECHO SQL client is missing 
	ECHO Check [SQLClient] value in Config.cfg
	GOTO END
)
SET SqlCmdString=%SQLClient% -f 65001 -b -S%DatabaseServer% -U%DatabaseUser% -P%DatabasePassword% -d%DatabaseDBName%

REM running scripts
ECHO Log from %DATE%_%TIME% > Run.log

FOR /F "tokens=*" %%i in (RunOrder.cfg) DO (	
	ECHO %%i >> con
	IF /I "%%i"=="DefaultData" (		
		IF /I "%RunDefaultData%"=="Yes" (
			CALL %FuncsDir%\CreateForFolder.bat %%i >> Run.log 2>&1
		)
	) ELSE (
		IF /I "%%i"=="DemoData" (			
			IF /I "%RunDemoData%"=="Yes" (
				CALL %FuncsDir%\CreateForFolder.bat %%i >> Run.log 2>&1
			)
		) ELSE (
			IF /I "%%i"=="Tables" (			
				IF /I "%RunUpdate%"=="No" CALL %FuncsDir%\CreateForFolder.bat %%i >> Run.log 2>&1		
			) ELSE (
				IF /I "%%i"=="Update" (			
					IF /I "%RunUpdate%"=="Yes" CALL %FuncsDir%\CreateForFolder.bat %%i >> Run.log 2>&1	
				) ELSE (
					IF /I "%%i"=="CreateDB" (
						IF /I "%RunCreateDB%"=="Yes" (
							IF /I "%RunUpdate%"=="No" CALL %FuncsDir%\CreateDBForFolder.bat %%i >> Run.log 2>&1
									     )
							
					) ELSE (
						CALL %FuncsDir%\CreateForFolder.bat %%i >> Run.log 2>&1
						)
				)
			)	
		)
	)
)


:END
ENDLOCAL
@ECHO ON