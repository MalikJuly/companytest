﻿IF OBJECT_ID('dbo.Position', 'U') IS NOT NULL
  DROP TABLE [dbo].[Position]; 
GO

CREATE TABLE [dbo].[Position] (
    [Id]    INT IDENTITY (1, 1) NOT NULL,
    [Title] NVARCHAR (50) NOT NULL
)
GO

ALTER TABLE [dbo].[Position] ADD CONSTRAINT [PK_Id] PRIMARY KEY ([Id])
GO

