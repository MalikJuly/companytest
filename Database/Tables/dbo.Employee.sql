﻿IF OBJECT_ID('dbo.Employee', 'U') IS NOT NULL
  DROP TABLE [dbo].[Employee]; 
GO

CREATE TABLE [dbo].[Employee] (
    [EmployeeID] INT           IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (50) NOT NULL,
    [Salary]     INT           NOT NULL,
    [Status]     TINYINT       NOT NULL,
    [PositionID] INT           NOT NULL
)
GO

ALTER TABLE [dbo].[Employee] ADD CONSTRAINT [PK_EmployeeID] PRIMARY KEY ([EmployeeID])
GO

