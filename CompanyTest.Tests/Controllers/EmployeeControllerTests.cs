﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CompanyTest.Controllers;
using CompanyTest.Models;

namespace CompanyTest.Controllers.Tests
{
    [TestClass()]
    public class EmployeeControllerTests
    {
        protected EmployeeController controller = new EmployeeController();

        [TestMethod()]
        public void IndexTest()
        {
            // Arrange
            string sortOrder = "";
            string currentFilter = "0";
            string searchString = currentFilter;
            int? page = null;
            // Act
            ViewResult result = controller.Index(sortOrder, currentFilter, searchString, page) as ViewResult;
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void CreateTest()
        {
            // Act
            ViewResult result = controller.Create() as ViewResult;
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void CreatePostTest()
        {
            // Arrange
            Employee employee = null;
            // Act
            ViewResult result = controller.Create(employee) as ViewResult;
            // Assert is handled by the ExpectedException
        }

        [TestMethod()]
        public void EditTest()
        {
            // Arrange
            int? id = null;
            // Act
            ViewResult result = controller.Edit(id) as ViewResult;
            // Assert is handled by the ExpectedException
        }

        [TestMethod()]
        public void EditTest1()
        {
            // Arrange
            int? id = -5;
            // Act
            var result = controller.Edit(id);
            // Assert is handled by the ExpectedException
        }

        [TestMethod()]
        public void EditTest2()
        {
            // Arrange
            int? id = 1;
            // Act
            var result = controller.Edit(id);
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void EditPostTest()
        {
            // Arrange
            int? id = null;
            // Act
            var result = controller.Edit(id);
            // Assert is handled by the ExpectedException
        }

        [TestMethod()]
        public void EditPostTest1()
        {
            // Arrange
            int? id = -5;
            // Act
            var result = controller.Edit(id);
            // Assert is handled by the ExpectedException
        }

        [TestMethod()]
        public void EditPostTest2()
        {
            //Arrange
            int? id = 1;
            // Act
            var result = controller.Edit(id);
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            // Arrange
            int? id = null;
            // Act
            ViewResult result = controller.Delete(id) as ViewResult;
            // Assert is handled by the ExpectedException
        }

        [TestMethod()]
        public void DeleteTest1()
        {
            // Arrange
            int? id = -5;
            // Act
            var result = controller.Delete(id);
            // Assert is handled by the ExpectedException
        }

        [TestMethod()]
        public void DeleteTest2()
        {
            //Arrange
            int? id = 1;
            // Act
            var result = controller.Delete(id);
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void DeletePostTest()
        {
            // Arrange
            int id = 0;
            // Act
            ViewResult result = controller.Delete(id) as ViewResult;
            // Assert is handled by the ExpectedException
        }

        [TestMethod()]
        public void DeletePostTest1()
        {
            // Arrange
            int id = 0;
            // Act
            var result = controller.Delete(id);
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void CreateReportTest()
        {
            // Act
            ViewResult result = controller.CreateReport() as ViewResult;
            // Assert is handled by the ExpectedException
        }

        [TestMethod()]
        public void CreateReportTest1()
        {
            // Act
            var result = controller.CreateReport();
            // Assert
            Assert.IsNotNull(result);
        }
    }
}