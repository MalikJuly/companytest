﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CompanyTest.Models
{
    public class Position
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Title cannot be longer than 50 characters.")]
        [Display(Name = "Title")]
        public string Title { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}