﻿using System.ComponentModel.DataAnnotations;

namespace CompanyTest.Models
{
    public class Employee
    {
        public int EmployeeID { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Salary")]
        public int Salary { get; set; }

        [Required]
        [Display(Name = "Status")]
        [Range(0, 5)]
        public byte Status { get; set; } = 0;

        [Required]
        [Display(Name = "Position")]
        public int PositionID { get; set; }

        public virtual Position Position { get; set; }
    }
}