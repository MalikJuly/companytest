﻿using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CompanyTest.DAL;
using CompanyTest.Models;
using PagedList;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using CompanyTest.Logging;

namespace CompanyTest.Controllers
{
    public class EmployeeController : Controller
    {
        private CompanyContext db = new CompanyContext();
        private Logger Log = new Logger();

        public class SelectStatus
        {
            public string Text { get; set; }
            public int Value { get; set; }
        }

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var statuses = new List<SelectStatus>();
            statuses.Add(new SelectStatus() { Value = 2, Text = "All" });
            statuses.Add(new SelectStatus() { Value = 1, Text = "Active" });
            statuses.Add(new SelectStatus() { Value = 0, Text = "Inactive" });
            ViewBag.Statuses = statuses;

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.SalarySortParm = sortOrder == "Salary" ? "salary_desc" : "Salary";

            if (searchString != null)
                page = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            var employees = from s in db.Employees
                            select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                var st = Convert.ToByte(searchString);
                if (st != 2)
                    employees = employees.Where(s => s.Status == st);
            }

            switch (sortOrder)
            {
                case "name_desc":
                    employees = employees.OrderByDescending(s => s.Name);
                    break;
                case "salary_desc":
                    employees = employees.OrderByDescending(s => s.Salary);
                    break;
                case "Salary":
                    employees = employees.OrderBy(s => s.Salary);
                    break;
                default:
                    employees = employees.OrderBy(s => s.Name);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(employees.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Create()
        {
            Log.Information(String.Format(@"Start Create New Employee at {0}", DateTime.Now.ToString()));
            var employee = new Employee();
            employee.Position = new Position();
            ViewBag.Positions = new SelectList(db.Positions.Select(x => new { x.Id, x.Title })
                .Distinct().OrderBy(x => x.Id), "Id", "Title");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,PositionID,Salary,Status")]Employee employee)
        {
            if (employee != null && ModelState.IsValid)
            {
                var position = db.Positions.Single(x => x.Id == employee.PositionID);
                employee.Position = position;
                db.Employees.Add(employee);
                db.SaveChanges();
                Log.Information(String.Format(@"New Employee with EmployeID = {0} & Name = {1} created at {2}", employee.EmployeeID, employee.Name, DateTime.Now.ToString()));
                return RedirectToAction("Index");
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Employee object must not be null and have correct properties values.");
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Employee object must not be null.");

            Employee employee = db.Employees.SingleOrDefault(x => x.EmployeeID == id);
            if (employee == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Employee not found.");

            ViewBag.Positions = new SelectList(db.Positions.Select(x => new { x.Id, x.Title })
            .Distinct().OrderBy(x => x.Id), "Id", "Title");
            return View(employee);
        }

        [HttpPost, Route("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Employee object must not be null.");

            var employeeToUpdate = db.Employees.Find(id);
            if (employeeToUpdate == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Employee not found.");

            if (TryUpdateModel(employeeToUpdate, "",
               new string[] { "Name", "PositionID", "Salary", "Status" }))
            {
                try
                {
                    db.SaveChanges();
                    Log.Information(String.Format(@"Employee with EmployeID = {0} updated at {1}", employeeToUpdate.EmployeeID, DateTime.Now.ToString()));
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    string Msg = ex.Message;
                    if (ex.InnerException != null)
                        Msg += ex.InnerException.Message;
                    Log.Error(Msg);
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            return View(employeeToUpdate);
        }

        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Employee object must not be null.");

            if (saveChangesError.GetValueOrDefault())
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";

            Employee employee = db.Employees.Find(id);
            if (employee == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Employee not found.");

            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Employee employee = db.Employees.Find(id);
                db.Employees.Remove(employee);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                string Msg = ex.Message;
                if (ex.InnerException != null)
                    Msg += ex.InnerException.Message;
                Log.Error(Msg);
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        public ActionResult CreateReport()
        {
            Employee[] employeesActive = db.Employees.Where(x => x.Status == 1).ToArray<Employee>();

            string percents = ""; double duty = 0; double result = 0; string Name = "Name: "; string Salary = "Salary: "; string Tax = "Tax "; string SalaryWithoutTax = "Salary - Tax: ";
            string[] report = new string[employeesActive.Length + 1];

            report[0] = "Report of active Employees for " + DateTime.Now.ToString("dd-MM-yyyy HH:mm");

            for (var i = 0; i < employeesActive.Length; i++)
            {
                if (employeesActive[i].Salary > 0 && employeesActive[i].Salary < 10000)
                {
                    percents = "10%: ";
                    duty = employeesActive[i].Salary / 10;
                    result = Math.Round((double)(employeesActive[i].Salary - duty), 2);
                }
                if (employeesActive[i].Salary >= 10000 && employeesActive[i].Salary <= 25000)
                {
                    percents = "15%: ";
                    duty = (employeesActive[i].Salary / 100) * 15;
                    result = Math.Round((double)(employeesActive[i].Salary - duty), 2);
                }
                if (employeesActive[i].Salary > 25000)
                {
                    percents = "25%: ";
                    duty = employeesActive[i].Salary / 4;
                    result = Math.Round((double)(employeesActive[i].Salary - duty), 2);
                }
                report[i + 1] = (i + 1) + ". " + Name + employeesActive[i].Name + ", " + Salary + employeesActive[i].Salary.ToString() + ", " + Tax + percents + duty.ToString() + ", " + SalaryWithoutTax + result.ToString() + ".";
            }
            try
            {
                Log.Information(String.Format(@"Save report of active employees to the file. Date - {0}", DateTime.Now.ToString("MM.dd.yyyy")));
                Helpers.FileHelper.SaveReportFile(report);
            }
            catch (Exception ex)
            {
                string Msg = ex.Message;
                if (ex.InnerException != null)
                    Msg += ex.InnerException.Message;
                Log.Error(Msg);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();

            base.Dispose(disposing);
        }
    }
}