﻿using CompanyTest.DAL;
using System.Web.Mvc;

namespace CompanyTest.Controllers
{
    public class HomeController : Controller
    {
        private CompanyContext db = new CompanyContext();

        public ActionResult Index()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}