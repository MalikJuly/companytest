﻿using CompanyTest.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CompanyTest.Helpers
{
    public class FileHelper
    {
        private static Dictionary<string, string> Paths = new Dictionary<string, string>
        {
            ["ActiveEmployeesReports"] = "/Reports/"
        };

        public static string PathToReportFile()
        {
            return HttpContext.Current.Server.MapPath("~") + Paths["ActiveEmployeesReports"]
                    + DateTime.Now.ToString("dd-MM-yyyy_HH-mm") + ".txt";
        }

        public static void SaveReportFile(string[] report)
        {
            string path = PathToReportFile();
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(PathToReportFile());
            if (!fileInfo.Directory.Exists)
                fileInfo.Directory.Create();

            if (!File.Exists(path))
            {
                try
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        for (int i = 0; i < report.Length; i++)
                        {
                            sw.WriteLine(report[i]);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string Msg = ex.Message;
                    if (ex.InnerException != null)
                        Msg += ex.InnerException.Message;
                    new Logger().Error(Msg);
                }
            }
        }

        public static void RemoveReportFile(string path)
        {
            if (File.Exists(path))
            {
                try
                {
                    System.IO.File.Delete(path);
                }
                catch (Exception ex)
                {
                    string Msg = ex.Message;
                    if (ex.InnerException != null)
                        Msg += ex.InnerException.Message;
                    new Logger().Error(Msg);
                }
            }
        }
    }
}